//
//  ViewController.swift
//  Test
//
//  Created by Kiroshan Thayaparan on 11/24/21.
//


import FBSDKLoginKit

class ViewController: UIViewController {
    
    var window: UIWindow?
    
    let facebookButton: UIButton = {
        let button = UIButton()
        button.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width-50, height: 50)
        button.setTitle("Continue with Facebook", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = .blue
        button.layer.cornerRadius = 10
        button.addTarget(self, action: #selector(facebookLogin), for: .touchUpInside)
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        facebookButton.center = view.center
        
        if let token = AccessToken.current,
            !token.isExpired {
            // User is logged in, do work such as go to next view controller.
            let token = token.tokenString

            let request = FBSDKLoginKit.GraphRequest(graphPath: "me", parameters: ["fields": "email, name"], tokenString: token, version: nil, httpMethod: .get)

            request.start(completion: { connection, result, error in
                let dict = result as! [String: AnyObject] as NSDictionary
                let name = dict.object(forKey: "name") as! String
                let email = dict.object(forKey: "email") as! String
                
                self.gotoHome(name: name, email: email)
            })
        } else {
            
            view.addSubview(facebookButton)
        }
    }
    
    @objc func facebookLogin() {
        let loginManager = LoginManager()
        loginManager.logIn(permissions: [.publicProfile], viewController: self) { (result) in
            switch result {
            case .cancelled:
                print("Clicked on cancel button")
            case .failed(let error):
                print(error.localizedDescription)
            case .success(_, _, _):
                self.getFacebookData()
            }
        }
    }
    
    func gotoHome(name: String, email: String) {
        let controller = HomeViewController()
        controller.name = name
        controller.email = email
        let navigationController = UINavigationController(rootViewController: controller)
        navigationController.modalPresentationStyle = .fullScreen
        UIApplication.shared.windows.first?.rootViewController = navigationController
        UIApplication.shared.windows.first?.window?.makeKeyAndVisible()
    }
    
    func getFacebookData() {
        if AccessToken.current != nil {
            GraphRequest(graphPath: "me", parameters: ["fields": "email, name"]).start {(connection, result, error) in
                if error == nil {
                    let dict = result as! [String: AnyObject] as NSDictionary
                    let name = dict.object(forKey: "name") as! String
                    let email = dict.object(forKey: "email") as! String
                    print("\(name)")
                    print("\(email)")
                    
                    self.gotoHome(name: name, email: email)
                } else {
                    print(error?.localizedDescription ?? "")
                }
            }
        } else {
            print("Access token is NIL")
        }
    }
}
