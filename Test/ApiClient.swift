//
//  ApiClient.swift
//  Test
//
//  Created by Kiroshan Thayaparan on 11/25/21.
//

import Alamofire
import SwiftyJSON
import Reachability

class ApiClient {
    
    internal func sendRequest(request_url: String, success: @escaping (_ data: AnyObject?, _ code: Int) -> Void, failure: @escaping (_ error: NSError) -> Void) {
        let url = URL(string:"\(request_url)")
        
        let parameters: Parameters = [:]
        let headers: HTTPHeaders = [:]
        
        request(url!, method: .get, parameter: parameters, headers: headers, success: { (data, code) -> Void in
            success(data, code)
        }) { (error) -> Void in
            failure(error)
        }
    }
    
    fileprivate func request(_ url: URL, method: HTTPMethod, parameter: Parameters, headers: HTTPHeaders, success: @escaping (_ data: AnyObject?, _ code: Int) -> Void, failure: @escaping (_ error: NSError) -> Void) {

        func callAPIrequest() {
            Alamofire.upload(multipartFormData: { multipartFormData in
                for (key, value) in parameter {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
            },to:url,method:method,
              headers: headers,
              encodingCompletion: { encodingResult in
                
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON{ response in
                        if let response = response.response {
                            let validateResult = HttpValidator.validate(response.statusCode)
                            print("validateResult ---- : \(validateResult)\(url)\(parameter)")
                        }
                        switch response.result {
                        case .success( let data):
                                      print("Data ---- : \(data)")
                            if let response = response.response {
                                let validateResult = HttpValidator.validate(response.statusCode)
                                success(data as AnyObject, validateResult.code)
                                
                            } else {
                                NSLog("No response for POST request: \(String(describing: response.request))")
                            }
                        case .failure(let error):
                            NSLog("API Failure: \(error)")
                        }
                    }.uploadProgress { progress in
                        print("Upload Progress: \(progress.fractionCompleted)")
                    }
                    return
                case .failure(let encodingError):
                    print(encodingError.localizedDescription, 500)
                    break
                }
            })
        }
        
        if !isNetworkAvailable() {
            print("Internet Status : No Internet")
        } else{
            callAPIrequest()
        }
    }
}


func isNetworkAvailable() -> Bool {
    return Reachability.forInternetConnection().isReachable()
}
