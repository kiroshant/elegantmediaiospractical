//
//  HotelInfoViewController.swift
//  Test
//
//  Created by Kiroshan Thayaparan on 11/25/21.
//

import UIKit

class HotelInfoViewController: UIViewController {
    
    var data: Hotel! = nil {
        didSet {
            avatar.kf.setImage(with: URL(string: data.image[2]))
            textViewTitle.text = data.title
            textViewAddress.text = data.description
        }
    }
    
    private var avatar: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.backgroundColor = .gray
        imageView.contentMode = .scaleToFill
        return imageView
    }()
    
    private var textViewTitle: UITextView = {
        let textView = UITextView()
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.isUserInteractionEnabled = false
        textView.textColor = .darkGray
        textView.text = "Title"
        textView.font = UIFont.boldSystemFont(ofSize: 14)
        textView.textAlignment = .center
        return textView
    }()
    
    private var textViewAddress: UITextView = {
        let textView = UITextView()
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.isUserInteractionEnabled = false
        textView.textColor = .darkGray
        textView.text = "Address"
        textView.font = UIFont.systemFont(ofSize: 12)
        return textView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        
        navigationController?.navigationBar.barTintColor = UIColor(red: 0.10, green: 0.74, blue: 0.61, alpha: 1.00)
        navigationController?.navigationBar.barStyle = .default
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "back_arrow").withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(backButtonTapped))
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "marker").withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(openMap))
        title = "Details"
        
        view.addSubview(avatar)
        view.addSubview(textViewTitle)
        view.addSubview(textViewAddress)
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewWillLayoutSubviews()
       
        avatar.leftAnchor.constraint(equalTo: view.leftAnchor, constant: view.frame.width/4).isActive = true
        avatar.topAnchor.constraint(equalTo: view.topAnchor, constant: view.safeAreaInsets.top).isActive = true
        avatar.widthAnchor.constraint(equalToConstant: view.frame.width/2).isActive = true
        avatar.heightAnchor.constraint(equalToConstant: view.frame.width/2).isActive = true
        
        textViewTitle.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        textViewTitle.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        textViewTitle.topAnchor.constraint(equalTo: avatar.bottomAnchor, constant: 10).isActive = true
        textViewTitle.bottomAnchor.constraint(equalTo: textViewAddress.topAnchor).isActive = true
        textViewTitle.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        textViewAddress.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 5).isActive = true
        textViewAddress.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -10).isActive = true
        textViewAddress.topAnchor.constraint(equalTo: textViewTitle.bottomAnchor, constant: 10).isActive = true
        textViewAddress.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 10).isActive = true
       
    }
    
    @objc func backButtonTapped() {
        dismiss(animated: true, completion: nil)
    }
    
    @objc func openMap() {
        let controller = MapViewController()
        controller.data = data
        let navigationController = UINavigationController(rootViewController: controller)
        navigationController.modalPresentationStyle = .fullScreen
        present(navigationController, animated: true, completion: nil)
    }
}
