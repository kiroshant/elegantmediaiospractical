//
//  HomeModel.swift
//  Test
//
//  Created by Kiroshan Thayaparan on 11/25/21.
//

import Foundation
import SwiftyJSON

class HomeModel {
    
    private let api = ApiClient()
    var hotelList: [Hotel] = []
    
    func getHotelData(getHotelDataCallFinished: @escaping (_ status: Bool) -> Void) {
        //get data from http request
        api.sendRequest(request_url: "https://dl.dropboxusercontent.com/s/6nt7fkdt7ck0lue/hotels.json", success: { (response, code) -> Void in
            
            if code == 200 {
                let currentData = JSON(response as Any)
                
                let res = currentData["data"].arrayObject
                
                let jsonArray = JSON(res as Any).array
                
                if let jsonList = jsonArray {
                    self.hotelList.removeAll()
                    for jsonObject in jsonList {
                        let hotel = Hotel(id: jsonObject["id"].int ?? 0,
                                          title: jsonObject["title"].string ?? "",
                                          description: jsonObject["description"].string ?? "",
                                          address: jsonObject["address"].string ?? "",
                                          postcode: jsonObject["postcode"].string ?? "",
                                          phoneNumber: jsonObject["phoneNumber"].string ?? "",
                                          latitude: jsonObject["latitude"].string ?? "",
                                          longitude: jsonObject["longitude"].string ?? "",
                                          image: [jsonObject["image"]["small"].string ?? "",
                                                  jsonObject["image"]["medium"].string ?? "",
                                                  jsonObject["image"]["large"].string ?? ""])
                        self.hotelList.append(hotel)
                    }
                }
                getHotelDataCallFinished(true)
            } else {
                print("Api status not equal 200")
                getHotelDataCallFinished(false)
            }
        }){ (error) -> Void in
            NSLog("Error (getHotelDataCallFinished): \(error.localizedDescription)")
            getHotelDataCallFinished(false)
        }
        
        //get data from json file
        
//        guard let path = Bundle.main.path(forResource: "list", ofType: "json") else { return }
//        let url = URL(fileURLWithPath: path)
//        do {
//            let data = try Data(contentsOf: url)
//            let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
//            let currentData = JSON(json as Any)
//            if currentData["status"] == 200 || currentData["status"] == "200" {
//
//                let res = currentData["data"].arrayObject
//
//                let jsonArray = JSON(res as Any).array
//
//                if let jsonList = jsonArray {
//                    self.hotelList.removeAll()
//                    for jsonObject in jsonList {
//                        let hotel = Hotel(id: jsonObject["id"].int ?? 0,
//                                          title: jsonObject["title"].string ?? "",
//                                          description: jsonObject["description"].string ?? "",
//                                          address: jsonObject["address"].string ?? "",
//                                          postcode: jsonObject["postcode"].string ?? "",
//                                          phoneNumber: jsonObject["phoneNumber"].string ?? "",
//                                          latitude: jsonObject["latitude"].string ?? "",
//                                          longitude: jsonObject["longitude"].string ?? "",
//                                          image: [jsonObject["image"]["small"].string ?? "",
//                                                  jsonObject["image"]["medium"].string ?? "",
//                                                  jsonObject["image"]["large"].string ?? ""])
//                        self.hotelList.append(hotel)
//                    }
//                }
//                getHotelDataCallFinished(true)
//            } else {
//                print("Api status not equal 200")
//                getHotelDataCallFinished(false)
//            }
//        } catch {
//            print("error")
//            getHotelDataCallFinished(false)
//        }
    }
}
