//
//  Hotel.swift
//  Test
//
//  Created by Kiroshan Thayaparan on 11/25/21.
//

class Hotel {

    var id: Int
    var title: String
    var description: String
    var address: String
    var postcode: String
    var phoneNumber: String
    var latitude: String
    var longitude: String
    var image: [String]
      
    init(id: Int, title: String, description: String, address: String, postcode: String, phoneNumber: String, latitude: String, longitude: String, image: [String]) {

        self.id = id
        self.title = title
        self.description = description
        self.address = address
        self.postcode = postcode
        self.phoneNumber = phoneNumber
        self.latitude = latitude
        self.longitude = longitude
        self.image = image
    }
}
