//
//  MapViewController.swift
//  Test
//
//  Created by Kiroshan Thayaparan on 11/25/21.
//

import GoogleMaps
import UIKit

class MapViewController: UIViewController {
    
    var data: Hotel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.barTintColor = UIColor(red: 0.10, green: 0.74, blue: 0.61, alpha: 1.00)
        navigationController?.navigationBar.barStyle = .default
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "back_arrow").withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(backButtonTapped))
        title = "Map"
        
        GMSServices.provideAPIKey("AIzaSyB8z95T-nYSIzW4y7zkAtulLP9NpeKzQak")
        
        let camera = GMSCameraPosition.camera(withLatitude: Double(data!.latitude) ?? -33.86, longitude: Double(data!.longitude) ?? 151.20, zoom: 6.0)
        let mapView = GMSMapView.map(withFrame: view.frame, camera: camera)
        view.addSubview(mapView)

        // Creates a marker in the center of the map.
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: Double(data!.latitude) ?? -33.86, longitude: Double(data!.longitude) ?? 151.20)
        marker.title = data!.title
        marker.snippet = data!.address
        marker.map = mapView
    }
    
    @objc func backButtonTapped() {
        dismiss(animated: true, completion: nil)
    }
}
