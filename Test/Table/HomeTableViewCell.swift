//
//  HomeTableViewCell.swift
//  Test
//
//  Created by Kiroshan Thayaparan on 11/25/21.
//

import UIKit
import Kingfisher

class HomeTableViewCell: UITableViewCell {
    
    var data: Hotel! = nil {
        didSet {
            avatar.kf.setImage(with: URL(string: data.image[0]))
            labelTitle.text = data.title
            textViewAddress.text = data.address
        }
    }
    
    private var avatar: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.layer.cornerRadius = 30
        imageView.clipsToBounds = true
        imageView.backgroundColor = .gray
        imageView.contentMode = .scaleToFill
        return imageView
    }()
    
    private var labelTitle: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .black
        label.text = "Title"
        label.font = UIFont.boldSystemFont(ofSize: 14)
        return label
    }()
    
    private var textViewAddress: UITextView = {
        let textView = UITextView()
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.isUserInteractionEnabled = false
        textView.textColor = .darkGray
        textView.text = "Address"
        textView.font = UIFont.systemFont(ofSize: 14)
        return textView
    }()
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.isUserInteractionEnabled = true
        commonInit(view: self)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func commonInit(view: UIView) {
        view.addSubview(avatar)
        view.addSubview(labelTitle)
        view.addSubview(textViewAddress)
        
        avatar.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 10).isActive = true
        avatar.topAnchor.constraint(equalTo: view.topAnchor, constant: 10).isActive = true
        avatar.widthAnchor.constraint(equalToConstant: 60).isActive = true
        avatar.heightAnchor.constraint(equalToConstant: 60).isActive = true
        
        labelTitle.leftAnchor.constraint(equalTo: avatar.rightAnchor, constant: 10).isActive = true
        labelTitle.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -10).isActive = true
        labelTitle.topAnchor.constraint(equalTo: view.topAnchor, constant: 10).isActive = true
        labelTitle.bottomAnchor.constraint(equalTo: textViewAddress.topAnchor).isActive = true
        labelTitle.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        textViewAddress.leftAnchor.constraint(equalTo: avatar.rightAnchor, constant: 5).isActive = true
        textViewAddress.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -10).isActive = true
        textViewAddress.topAnchor.constraint(equalTo: labelTitle.bottomAnchor, constant: 10).isActive = true
        textViewAddress.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 10).isActive = true
    }
}
