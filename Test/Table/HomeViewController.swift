//
//  HomeViewController.swift
//  Test
//
//  Created by Kiroshan Thayaparan on 11/25/21.
//

import UIKit
import FBSDKLoginKit

class HomeViewController: UIViewController {
    
    var name: String?
    var email: String?
    
    private var homeModel = HomeModel()
    private var hotelList: [Hotel] = []
    
    let tableView: UITableView = {
        let tableView = UITableView()
        tableView.register(HomeTableViewCell.self, forCellReuseIdentifier: "HomeTableViewCell")
        return tableView
    }()
    
    let homeTableHeaderView = HomeTableHeaderView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 100))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        title = "List View"
        navigationController?.navigationBar.barTintColor = UIColor(red: 0.10, green: 0.74, blue: 0.61, alpha: 1.00)
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = nil
        //tableView.allowsSelection = false
        tableView.backgroundColor = .white
        view.addSubview(tableView)
        
        homeTableHeaderView.data = [name ?? "", email ?? ""]
        homeTableHeaderView.buttonLogout.addTarget(self, action: #selector(facebookLogout), for: .touchUpInside)
        tableView.tableHeaderView = homeTableHeaderView
        
        getHotelData()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableView.frame = view.bounds
        
    }
    
    @objc func facebookLogout() {
        let loginManager = LoginManager()
        loginManager.logOut()
        
        let controller = ViewController()
        //let navigationController = UINavigationController(rootViewController: controller)
        controller.modalPresentationStyle = .fullScreen
        UIApplication.shared.windows.first?.rootViewController = controller
        UIApplication.shared.windows.first?.window?.makeKeyAndVisible()
    }
    
    func getHotelData() {
        homeModel.getHotelData(getHotelDataCallFinished: { (status) in
            if status {
                self.hotelList.append(contentsOf: self.homeModel.hotelList)
            }
            self.tableView.reloadData()
        })
    }
}

extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return hotelList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCell(withIdentifier: "HomeTableViewCell", for: indexPath) as! HomeTableViewCell
        cell.selectionStyle = .none
        cell.data = hotelList[indexPath.row]
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let controller = HotelInfoViewController()
        controller.data = hotelList[indexPath.row]
        let navigationController = UINavigationController(rootViewController: controller)
        navigationController.modalPresentationStyle = .fullScreen
        present(navigationController, animated: true, completion: nil)
    }
}
