//
//  HomeTableHeaderView.swift
//  Test
//
//  Created by Kiroshan Thayaparan on 11/25/21.
//

import UIKit

class HomeTableHeaderView: UIView {
    
    var data: [String] = [] {
        didSet {
            labelName.text = data[0]
            labelEmail.text = data[1]
        }
    }
    
    private var labelName: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.text = "Name"
        label.font = UIFont.systemFont(ofSize: 15)
        label.textAlignment = .center
        return label
    }()
    
    private var labelEmail: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.text = "Email"
        label.font = UIFont.systemFont(ofSize: 13)
        label.textAlignment = .center
        return label
    }()
    
    var buttonLogout: UIButton = {
        let button = UIButton()
        button.setTitle("Logout", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = UIColor(red: 0.10, green: 0.74, blue: 0.61, alpha: 1.00)
        button.layer.cornerRadius = 5
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit(view: self)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func commonInit(view: UIView) {
        labelName.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 30)
        labelEmail.frame = CGRect(x: 0, y: 30, width: view.frame.width, height: 20)
        buttonLogout.frame = CGRect(x: 0, y: 60, width: view.frame.width/2, height: 30)
        buttonLogout.center.x = view.center.x
        
        view.addSubview(labelName)
        view.addSubview(labelEmail)
        view.addSubview(buttonLogout)
    }
}
